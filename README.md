# Description
This script pulls every project and repo under a specified group and updates the `.gitlab-ci.yml` files. T-mobile is updating everything to run off of a `main` branch, rather than master. So it regexes the yml files and anywhere it finds `master` it replaces it with `main`

# How to run
- Make sure you have Python3 installed
- Clone the repo
- Run `# pip3 install --upgrade python-gitlab`

## In the `fix_yml.py` file you need to make some changes
- On this line, fill in the private token with a GitLab private token you create. It needs to have API permissions 

    - `with gitlab.Gitlab("https://gitlab.com", private_token="") as gl:`

- This line needs to consist of a list of the actual groups you want to edit. You find those by going to your groups main page in GitLab and reading the number.
    - `group_list = []`
    - For example, if you visit Hg's group (https://gitlab.com/tmobile/rf360/hg) you can see "Group ID: 7005886" at the top of the page. These are the group IDs you need to provide.
    - Example list of several groups under RF360:
        - `group_list = [7005842, 7005788, 7005706, 6002549,  6268967]`