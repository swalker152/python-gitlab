import gitlab
import subprocess
import re
import os
import os.path as path

# Replace every instance of the word 'master' with the word 'main' in all '.gitlab-ci.yml' files.
# Delete the "ref: master" lines.
# Then commit and push the branch, then open an MR at the project level in GitLab.
def edit_files(file_name):
    # file_name: path of the file to edit
    file_path = '/Users/SWalker152/src/repos/python-gitlab/' + file_name + '/.gitlab-ci.yml'
    if (path.isfile(file_path) == True):
        os.chdir('/Users/SWalker152/src/repos/python-gitlab/' + file_name)
        # subprocess.call(['git', 'checkout', 'tmo/main'])
        # subprocess.call(['git', 'branch', '-d', 'tmo_main_update'])
        # subprocess.call(['git', 'push', 'origin', '--delete', 'tmo_main_update'])
        subprocess.call(['git', 'checkout', '-b', 'tmo_main_update'])
        # switch in case the branch already existed
        subprocess.call(['git', 'checkout', 'tmo_main_update'])
        pattern1 = re.compile(r'ref:.*\n +')
        pattern2 = re.compile(r'master')
        with open(file_path, 'r') as f:
            text = f.read()
            f.close()
            new_text = re.sub(pattern1, '', text)
            new_text2 = re.sub(pattern2, 'tmo/main', new_text)

        with open(file_path, 'w') as f:
            f.write(new_text2)
    
        mr_project = gl.projects.get(project.id, lazy=True)
        subprocess.call(['git', 'add', '.'])
        subprocess.call(['git', 'commit', '-m' + "commit master to main changes"])
        subprocess.call(['git', 'push', '--set-upstream', 'origin', 'tmo_main_update'])
        mr = mr_project.mergerequests.create({'source_branch':'tmo_main_update', 'target_branch':'tmo/main', 'title': file_name + ' update tmo/master to tmo/main', })
        mr.description = "Updating yml files to point at main"
        mr.labels = ['platform team yml update']
        mr.save()
        os.chdir('/Users/SWalker152/src/repos/python-gitlab/')

    else: 
        print("Path " + file_path + " doesn't exist. Please check it manually.")

# private token auth
with gitlab.Gitlab("https://gitlab.com", private_token="") as gl:
    gl.auth()

    file_paths = []
    # Grab all groups under RF360 > then all projects > then search the repos for .gitlab-ci.yml files to change.
    # If files are found clone the repo and send to the edit function
    # group_list = [7005842, 7005788, 7005706, 6002549,  6268967] #rfds is last
    # for group in group_list:

    group = gl.groups.get(7005886) #hg group
    for project in group.projects.list(as_list = False):
        print(project.name, project.id, project.ssh_url_to_repo)
        repo = gl.projects.get(project.id)
        tree_list = repo.repository_tree(recursive = True, all = True)
        for item in tree_list:
            if item["name"] == ".gitlab-ci.yml":
                subprocess.call(['git', 'clone', project.ssh_url_to_repo])
                file_paths.append(repo.path)
                edit_files(repo.path)
# edit_files('export-service')




